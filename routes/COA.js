const router = require('express').Router()
const COAService = require('../services/COA.service')

router.post('/', COAService.createCOA)
router.get('/', COAService.readCOA)
router.put('/', COAService.updateCOA)
router.delete('/', COAService.deleteCOA)

module.exports = router