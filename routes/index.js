const router = require('express').Router()
const COARouter = require('./COA')
const jurnalRouter = require('./jurnal')

router.use('/COA', COARouter)
router.use('/jurnal', jurnalRouter)

module.exports = router