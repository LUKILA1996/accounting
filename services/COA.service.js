const fs = require('fs')

const dataCOA = './datas/COA.json'

const createCOA = (req, res) => {
  const { kode_akun, nama_akun, debit, credit } = req.body

  fs.readFile(dataCOA, 'utf-8', (err, data) => {
    if (err) res.status(500).json(err)

    const COA = JSON.parse(data)

    const isFind = COA.find((row) => row.kode_akun === kode_akun)

    const isFind2 = COA.find((row) => row.nama_akun === nama_akun)

    if (isFind) {
      res.status(400).json({ message: `kode akun is already available` })
    } else if (isFind2) {
      res.status(400).json({ message: `nama akun is already available` })
    } else if (kode_akun.length !== 8) {
      res.status(400).json({ message: `kode_akun must be 8 digits` })
    } else if (kode_akun.slice(0, 1) === '1' && debit !== true) {
      res.status(400).json({ message: `normal balance debit must true value` })
    } else if (kode_akun.slice(0, 1) === '2' && credit !== true) {
      res.status(400).json({ message: `normal balance debit must true value` })
    } else if (kode_akun.slice(0, 1) === '3' && credit !== true) {
      res.status(400).json({ message: `normal balance debit must true value` })
    } else if (kode_akun.slice(0, 1) === '4' && credit !== true) {
      res.status(400).json({ message: `normal balance debit must true value` })
    } else if (kode_akun.slice(0, 1) === '5' && debit !== true) {
      res.status(400).json({ message: `normal balance debit must true value` })
    } else {
      const payload = {
        ...req.body,
      }

      const newArray = [...COA, payload]

      fs.writeFile(dataCOA, JSON.stringify(newArray), 'utf-8', (err) => {
        res.status(200).json({ message: `COA created` })
      })
    }
  })
}

const readCOA = (req, res) => {
  fs.readFile(dataCOA, 'utf-8', (err, data) => {
    if (err) {
      res.status(500).json(err)
    } else {
      res
        .status(200)
        .json({ message: `get data COA success`, data: JSON.parse(data) })
    }
  })
}

const updateCOA = (req, res) => {
  const { kode_akun, nama_akun, debit, credit } = req.body
  fs.readFile(dataCOA, 'utf-8', (err, data) => {
    if (err) res.status(500).json(err)

    const COA = JSON.parse(data)

    const isFind = COA.find((row) => row.kode_akun === kode_akun)

    const isFind2 = COA.find((row) => row.nama_akun === nama_akun)

    if (!isFind) {
      res.status(400).json({ message: `kode akun is not found` })
    } else if (isFind2) {
      res.status(400).json({ message: `nama akun is already available` })
    } else if (kode_akun.length !== 8) {
      res.status(400).json({ message: `kode akun must be 8 digits` })
    } else if (kode_akun.slice(0, 1) === '1' && debit !== true) {
      res.status(400).json({ message: `normal balance debit must true value` })
    } else if (kode_akun.slice(0, 1) === '2' && credit !== true) {
      res.status(400).json({ message: `normal balance credit must true value` })
    } else if (kode_akun.slice(0, 1) === '3' && credit !== true) {
      res.status(400).json({ message: `normal balance credit must true value` })
    } else if (kode_akun.slice(0, 1) === '4' && credit !== true) {
      res.status(400).json({ message: `normal balance credit must true value` })
    } else if (kode_akun.slice(0, 1) === '5' && debit !== true) {
      res.status(400).json({ message: `normal balance debit must true value` })
    } else {
      const payload = {
        ...req.body,
      }

      const newArray = COA.map((row) => row.kode_akun === kode_akun ? payload : row)

      fs.writeFile(dataCOA, JSON.stringify(newArray), 'utf-8', () => {
        res.status(200).json({message: 'COA updated'})
      })
    }
  })
}

const deleteCOA = (req, res) => {
    const {kode_akun} = req.body
    fs.readFile(dataCOA, 'utf-8', (err, data) => {
        if (err) res.status(500).json(err)

        const COA = JSON.parse(data)

        const isFind = COA.find((row) => row.kode_akun === kode_akun)

        if (!isFind) {
            res.status(400).json({message: `kode akun is not found`})
        } else {
            const newArray = COA.filter((row) => row.kode_akun !== kode_akun)

            fs.writeFile(dataCOA, JSON.stringify(newArray), 'utf-8', () => {
                res.status(200).json({message: `COA deleted`})
            })
        }
    })
}

module.exports = { createCOA, readCOA, updateCOA, deleteCOA }
